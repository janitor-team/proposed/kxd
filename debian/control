Source: kxd
Section: net
Priority: optional
Maintainer: Maximiliano Curia <maxy@debian.org>
Uploaders: Alberto Bertogli <albertito@blitiri.com.ar>
Build-Depends: debhelper-compat (= 13),
               golang-any,
               python3,
               python3-docutils
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://blitiri.com.ar/p/kxd
Vcs-Browser: https://salsa.debian.org/debian/kxd
Vcs-Git: https://salsa.debian.org/debian/kxd.git

Package: kxc
Architecture: any
Depends: cryptsetup, kxgencert, ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Key exchange daemon -- client
 kxd is a key exchange daemon, which serves blobs of data (keys) over https.
 .
 It can be used to get keys remotely instead of using local storage. The main
 use case is to get keys to open dm-crypt devices automatically, without having
 to store them on the local machine.
 .
 This package provides the client part of kxd.

Package: kxd
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: kxgencert, ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Key exchange daemon
 kxd is a key exchange daemon, which serves blobs of data (keys) over https.
 .
 It can be used to get keys remotely instead of using local storage. The main
 use case is to get keys to open dm-crypt devices automatically, without having
 to store them on the local machine.

Package: kxgencert
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Key exchange daemon -- certificate generator
 kxd is a key exchange daemon, which serves blobs of data (keys) over https.
 .
 It can be used to get keys remotely instead of using local storage. The main
 use case is to get keys to open dm-crypt devices automatically, without having
 to store them on the local machine.
 .
 This package provides a certificate generator, for convenience when setting up
 the client and server.
